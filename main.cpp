/*** Include ***/
/* for general */
#include <cstdint>
#include <cstdlib>
#include <string>
#include <algorithm>
#include <chrono>
#include <map>
/* for OpenCV */
#include <opencv2/opencv.hpp>

/* for My modules */
#include "person_tracking.h"
#include "common_helper_cv.h"

/*** Macro ***/
#define WORK_DIR                      RESOURCE_DIR
#define DEFAULT_INPUT_IMAGE           RESOURCE_DIR"/kite.jpg"
#define NUM_THREAD                    4
/*** Function ***/
int32_t main(int argc, char* argv[])
{
    /* Find source image */
    std::string input_name = (argc > 1) ? argv[1] : DEFAULT_INPUT_IMAGE;
    cv::VideoCapture cap;   /* if cap is not opened, src is still image */
    if (!CommonHelper::FindSourceImage(input_name, cap)) {
        return -1;
    }

    /* Create video writer to save output video */
    cv::VideoWriter writer;
    //writer = cv::VideoWriter("out.mp4", cv::VideoWriter::fourcc('M', 'P', '4', 'V'), (std::max)(20.0, cap.get(cv::CAP_PROP_FPS)), cv::Size(static_cast<int32_t>(cap.get(cv::CAP_PROP_FRAME_WIDTH)), static_cast<int32_t>(cap.get(cv::CAP_PROP_FRAME_HEIGHT))));

    /* Initialize image processor library */
    PersonTracking::InputParam input_param ={WORK_DIR, NUM_THREAD};
    if (PersonTracking::Initialize(input_param) != 0) {
        printf("Initialization Error\n");
        return -1;
    }

    /*** Process for each frame ***/
    int32_t frame_cnt = 0;
    for (frame_cnt = 0; cap.isOpened(); frame_cnt++) {
        /* Read image */
        cv::Mat image;
        if (cap.isOpened()) {
            cap.read(image);
        } else {
            image = cv::imread(input_name);
        }
        if (image.empty()) break;
        /* Call image processor library */
        std::map<std::string,std::vector<PersonTracking::Result>> result_map;

        PersonTracking::Process(image, result_map);
        /* Display result */
        if (writer.isOpened()) writer.write(image);
        cv::imshow("person tracking with bytetrack and reid", image);

        /* Input key command */
        if (cap.isOpened()) {
            /* this code needs to be before calculating processing time because cv::waitKey includes image output */
            /* however, when 'q' key is pressed (cap.released()), processing time significantly incraeases. So escape from the loop before calculating time */
            if (CommonHelper::InputKeyCommand(cap)) break;
        };
    }
    
    /* Fianlize image processor library */
    PersonTracking::Finalize();
    if (writer.isOpened()) writer.release();
    cv::waitKey(-1);
    return 0;
}