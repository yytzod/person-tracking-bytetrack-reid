#pragma once

#include "STrack.h"
#include "feature_engine.h"
#include <chrono>
struct Object
{
    cv::Rect_<float> rect;
    int label;
    float prob;
};

class BYTETracker
{
public:
	BYTETracker(int frame_rate = 30, int track_buffer = 30,std::string reid_model_dir = "./resource/",int32_t reid_num_threads=4);
	~BYTETracker();

	vector<STrack> update(Mat& mat,const vector<Object>& objects,double& reid_time_consumed);
	int get_features(cv::Mat& mat, vector<float>& tlwh,FeatureEngine::Result& feature_result,bool is_query);
	Scalar get_color(int idx);

private:
	vector<STrack*> joint_stracks(vector<STrack*> &tlista, vector<STrack> &tlistb);
	vector<STrack> joint_stracks(vector<STrack> &tlista, vector<STrack> &tlistb);
	
	vector<STrack> sub_stracks(vector<STrack> &tlista, vector<STrack> &tlistb);
	void remove_duplicate_stracks(vector<STrack> &resa, vector<STrack> &resb, vector<STrack> &stracksa, vector<STrack> &stracksb);

	void linear_assignment(vector<vector<float> > &cost_matrix, int cost_matrix_size, int cost_matrix_size_size, float thresh,
		vector<vector<int> > &matches, vector<int> &unmatched_a, vector<int> &unmatched_b);
	vector<vector<float> > iou_distance(vector<STrack*> &atracks, vector<STrack> &btracks, int &dist_size, int &dist_size_size);
	vector<vector<float> > iou_distance(vector<STrack> &atracks, vector<STrack> &btracks);
	vector<vector<float> > ious(vector<vector<float> > &atlbrs, vector<vector<float> > &btlbrs);

	double lapjv(const vector<vector<float> > &cost, vector<int> &rowsol, vector<int> &colsol, 
		bool extend_cost = false, float cost_limit = LONG_MAX, bool return_cost = true);
	float cosine_similarity(const std::vector<float>& feature0, const std::vector<float>& feature1);
	bool check_bbox_validitity(vector<float>bbox,int img_w, int img_h);
	bool check_range_by_euclidean_distance_rois(std::vector<float>&bbox_1,std::vector<float>&bbox_2,float distance_threshold,int im_w,int im_h);
private:

	float track_thresh;
	float high_thresh;
	float match_thresh;
	float threshold_similarity;
	float distance_thresh_ratio;
	int frame_id;
	int max_time_lost;
	bool is_initial;

	vector<STrack> tracked_stracks;
	vector<STrack> lost_stracks;
	vector<STrack> removed_stracks;
	byte_kalman::KalmanFilter kalman_filter;
	FeatureEngine s_feature_engine;
};