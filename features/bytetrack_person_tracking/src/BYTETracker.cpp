#include "BYTETracker.h"
#include <fstream>
#include <algorithm>

BYTETracker::BYTETracker(int frame_rate, int track_buffer,std::string reid_model_dir,int32_t reid_num_threads)
{
	track_thresh = 0.5;
	high_thresh = 0.6;
	match_thresh = 0.8;
	threshold_similarity = 0.5;
	distance_thresh_ratio = 0.3;
	frame_id = 0;
	is_initial= true;
	max_time_lost = int(frame_rate / 30.0 * track_buffer);
	cout << "Init ByteTrack!" << endl;
	
	s_feature_engine.Initialize(reid_model_dir,reid_num_threads);
}

BYTETracker::~BYTETracker()
{
}
int BYTETracker::get_features(Mat& mat, vector<float>& tlwh, FeatureEngine::Result& feature_result,bool is_query){
	int x = tlwh[0]; int y = tlwh[1];
	int w = tlwh[2]; int h = tlwh[3];
	std::vector<int> bbox = {x,y,w,h};
	return this->s_feature_engine.Process(mat, bbox, feature_result,is_query);
}
vector<STrack> BYTETracker::update(cv::Mat& mat,const vector<Object>& objects,double& reid_time_consumed)
{
	////////////////// Step 1: Get detections //////////////////
	this->frame_id++;
	vector<STrack> activated_stracks;
	vector<STrack> refind_stracks;
	vector<STrack> removed_stracks;
	vector<STrack> lost_stracks;
	vector<STrack> detections;
	vector<STrack> detections_low;

	vector<STrack> detections_cp;
	vector<STrack> tracked_stracks_swap;
	vector<STrack> resa, resb;
	vector<STrack> output_stracks;

	vector<STrack*> unconfirmed;
	vector<STrack*> tracked_stracks;
	vector<STrack*> strack_pool;
	vector<STrack*> r_tracked_stracks;

	if (objects.size() > 0)
	{
		for (int i = 0; i < objects.size(); i++)
		{
			vector<float> tlbr_;
			tlbr_.resize(4);
			tlbr_[0] = objects[i].rect.x;
			tlbr_[1] = objects[i].rect.y;
			tlbr_[2] = objects[i].rect.x + objects[i].rect.width;
			tlbr_[3] = objects[i].rect.y + objects[i].rect.height;
			
			float score = objects[i].prob;

			STrack strack(STrack::tlbr_to_tlwh(tlbr_), score);
			if (score >= track_thresh)
			{
				detections.push_back(strack);
			}
			else
			{
				detections_low.push_back(strack);
			}
			
		}
	}

	// Add newly detected tracklets to tracked_stracks
	for (int i = 0; i < this->tracked_stracks.size(); i++)
	{
		if (!this->tracked_stracks[i].is_activated)
			unconfirmed.push_back(&this->tracked_stracks[i]);
		else
			tracked_stracks.push_back(&this->tracked_stracks[i]);
	}

	////////////////// Step 2: First association, with IoU //////////////////
	strack_pool = joint_stracks(tracked_stracks, this->lost_stracks);
	STrack::multi_predict(strack_pool, this->kalman_filter);

	vector<vector<float> > dists;
	int dist_size = 0, dist_size_size = 0;
	dists = iou_distance(strack_pool, detections, dist_size, dist_size_size);

	vector<vector<int> > matches;
	vector<int> u_track, u_detection;
	linear_assignment(dists, dist_size, dist_size_size, match_thresh, matches, u_track, u_detection);

	for (int i = 0; i < matches.size(); i++)
	{
		STrack *track = strack_pool[matches[i][0]];
		STrack *det = &detections[matches[i][1]];
		if (track->state == TrackState::Tracked)
		{
			track->update(*det, this->frame_id);
			activated_stracks.push_back(*track);
		}
		else
		{
			track->re_activate(*det, this->frame_id, false);
			refind_stracks.push_back(*track);
		}
	}

	////////////////// Step 3: Second association, using low score dets //////////////////
	for (int i = 0; i < u_detection.size(); i++)
	{
		detections_cp.push_back(detections[u_detection[i]]);
	}
	detections.clear();
	detections.assign(detections_low.begin(), detections_low.end());
	
	for (int i = 0; i < u_track.size(); i++)
	{
		if (strack_pool[u_track[i]]->state == TrackState::Tracked)
		{
			r_tracked_stracks.push_back(strack_pool[u_track[i]]);
		}
	}

	dists.clear();
	dists = iou_distance(r_tracked_stracks, detections, dist_size, dist_size_size);

	matches.clear();
	u_track.clear();
	u_detection.clear();
	linear_assignment(dists, dist_size, dist_size_size, 0.5, matches, u_track, u_detection);

	for (int i = 0; i < matches.size(); i++)
	{
		STrack *track = r_tracked_stracks[matches[i][0]];
		STrack *det = &detections[matches[i][1]];
		if (track->state == TrackState::Tracked)
		{
			track->update(*det, this->frame_id);
			activated_stracks.push_back(*track);
		}
		else
		{
			track->re_activate(*det, this->frame_id, false);
			refind_stracks.push_back(*track);
		}
	}

	double time_inference_lost_features=0;
	
	for (int i = 0; i < u_track.size(); i++)
	{
		STrack *track = r_tracked_stracks[u_track[i]];
		if (track->state != TrackState::Lost)
		{
			track->mark_lost();
			if(!(this->check_bbox_validitity(track->tlwh,mat.cols,mat.rows)))
				continue;
			FeatureEngine::Result feature_res;
			if(this->get_features(mat,track->tlwh,feature_res,false) == FeatureEngine::kRetOk){
				track->features = feature_res.feature;
				time_inference_lost_features += feature_res.time_inference;
			}	
			lost_stracks.push_back(*track);
		}
	}
	//std::cout<<"Time taken to extract current lost track features :"<<time_inference_lost_features<<" [ms]"<<std::endl;

	// Deal with unconfirmed tracks, usually tracks with only one beginning frame
	detections.clear();
	detections.assign(detections_cp.begin(), detections_cp.end());

	dists.clear();
	dists = iou_distance(unconfirmed, detections, dist_size, dist_size_size);

	matches.clear();
	vector<int> u_unconfirmed;
	u_detection.clear();
	linear_assignment(dists, dist_size, dist_size_size, 0.7, matches, u_unconfirmed, u_detection);

	for (int i = 0; i < matches.size(); i++)
	{
		unconfirmed[matches[i][0]]->update(detections[matches[i][1]], this->frame_id);
		activated_stracks.push_back(*unconfirmed[matches[i][0]]);
	}

	for (int i = 0; i < u_unconfirmed.size(); i++)
	{
		STrack *track = unconfirmed[u_unconfirmed[i]];
		track->mark_removed();
		removed_stracks.push_back(*track);
	}

	////////////////// Step 4: Init new stracks //////////////////
	vector<STrack*> query_boxes;
	vector<int> query_u_indices;
	vector<int> features_activated_indices;
	double time_inference_feature = 0;
	for (int i = 0; i < u_detection.size(); i++)
	{
		STrack *det = &detections[u_detection[i]];
		if (det->score < this->high_thresh)
			continue;
		
		if(!is_initial){
			FeatureEngine::Result feature_res;
			if(this->get_features(mat,det->tlwh,feature_res,true) == FeatureEngine::kRetOk){
				det->features = feature_res.feature;
				time_inference_feature += feature_res.time_inference;
				query_boxes.push_back(det);	
				query_u_indices.push_back(u_detection[i]);	
			}
			
		}
	}
	
	// std::cout<<"Time taken to extract query features: "<<time_inference_feature<<" [ms]"<<std::endl<<std::endl;
	auto calculate_similarity_time_start = std::chrono::steady_clock::now();
	if(query_boxes.size()!=0 && this->lost_stracks.size() != 0){

		std::vector<std::vector<float>> feature_simililarity_matrix(query_boxes.size(),std::vector<float>(this->lost_stracks.size(),0.0f));
		for (int i = 0; i< query_boxes.size();i++){
			for (int j = 0; j < this->lost_stracks.size(); j++)
			{	
				if(!(this->check_range_by_euclidean_distance_rois(query_boxes[i]->tlwh,this->lost_stracks[j].tlwh,distance_thresh_ratio*(mat.cols+mat.rows)/2,mat.cols,mat.rows)))
					continue;
				feature_simililarity_matrix[i][j]= cosine_similarity(query_boxes[i]->features,this->lost_stracks[j].features);
				// std::cout<<feature_simililarity_matrix[i][j]<<" ";
			}
			// std::cout<<std::endl;
		}
		
		vector<vector<int>> indices_of_matches;
		for(int query_idx=0;query_idx<feature_simililarity_matrix.size();query_idx++){
			vector<float> max_similarity_by_row = {feature_simililarity_matrix[query_idx][0],query_idx,0};	
			for(int gallery_idx=0;gallery_idx<feature_simililarity_matrix[0].size();gallery_idx++){
				if(max_similarity_by_row[0] < feature_simililarity_matrix[query_idx][gallery_idx]){
					max_similarity_by_row[0] = feature_simililarity_matrix[query_idx][gallery_idx];
					max_similarity_by_row[2] = float(gallery_idx);
				}
			}
			
			if(max_similarity_by_row[0]>threshold_similarity){
				indices_of_matches.push_back({int(max_similarity_by_row[1]),int(max_similarity_by_row[2])});
			}
		}
		for(int i=0; i<indices_of_matches.size();i++){
			STrack *det = query_boxes[indices_of_matches[i][0]];
			STrack *track = &this->lost_stracks[indices_of_matches[i][1]];
			track->re_activate(*det, this->frame_id);
			refind_stracks.push_back(*track);
			features_activated_indices.push_back(indices_of_matches[i][0]);
		}
	}

	auto calculate_similarity_time_end = std::chrono::steady_clock::now();
	double similarity_time_taken = static_cast<std::chrono::duration<double>>(calculate_similarity_time_end - calculate_similarity_time_start).count() * 1000.0;
	// std::cout<<"Similarity time taken: "<<similarity_time_taken<<" [ms]"<<std::endl;
	
	/** total time taken to process reid things **/
	reid_time_consumed = time_inference_lost_features + time_inference_feature + similarity_time_taken;
	
	/** Assign new ids to the rest of the undetected boxes (after reid) **/	
	for(int i=0; i<query_u_indices.size();i++){
		if ( (std::find(features_activated_indices.begin(), features_activated_indices.end(), query_u_indices[i]) != features_activated_indices.end()))
			continue;
		STrack *track = &detections[query_u_indices[i]];
		if(track->score < this->high_thresh)
			continue;
		track->activate(this->kalman_filter, this->frame_id);
		activated_stracks.push_back(*track);
	}
	
	////////////////// Step 5: Update state //////////////////
	
	for (int i = 0; i < this->lost_stracks.size(); i++)
	{
		if (this->frame_id - this->lost_stracks[i].end_frame() > this->max_time_lost)
		{
			this->lost_stracks[i].mark_removed();
			removed_stracks.push_back(this->lost_stracks[i]);
		}
	}
	
	for (int i = 0; i < this->tracked_stracks.size(); i++)
	{
		if (this->tracked_stracks[i].state == TrackState::Tracked)
		{
			tracked_stracks_swap.push_back(this->tracked_stracks[i]);
		}
	}
	this->tracked_stracks.clear();
	this->tracked_stracks.assign(tracked_stracks_swap.begin(), tracked_stracks_swap.end());

	this->tracked_stracks = joint_stracks(this->tracked_stracks, activated_stracks);
	this->tracked_stracks = joint_stracks(this->tracked_stracks, refind_stracks);

	//std::cout << activated_stracks.size() << std::endl;

	this->lost_stracks = sub_stracks(this->lost_stracks, this->tracked_stracks);
	for (int i = 0; i < lost_stracks.size(); i++)
	{
		this->lost_stracks.push_back(lost_stracks[i]);
	}

	this->lost_stracks = sub_stracks(this->lost_stracks, this->removed_stracks);
	for (int i = 0; i < removed_stracks.size(); i++)
	{
		this->removed_stracks.push_back(removed_stracks[i]);
	}
	
	remove_duplicate_stracks(resa, resb, this->tracked_stracks, this->lost_stracks);

	this->tracked_stracks.clear();
	this->tracked_stracks.assign(resa.begin(), resa.end());
	this->lost_stracks.clear();
	this->lost_stracks.assign(resb.begin(), resb.end());
	
	for (int i = 0; i < this->tracked_stracks.size(); i++)
	{
		if (this->tracked_stracks[i].is_activated)
		{
			output_stracks.push_back(this->tracked_stracks[i]);
		}
	}
	is_initial = false;
	return output_stracks;
}