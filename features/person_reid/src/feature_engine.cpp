/** 
Copyright 2021 iwatake2222

Licensed under the Apache License, Version 2.0 (the "License").
you may not use this file except in compliance with the License.

You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0


Modified by - Ye Yint Thu (yeyintthu@globalwalkers.co.jp)
==============================================================================*/

/*** Include ***/
/* for general */
#include <cstdint>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <string>
#include <vector>
#include <array>
#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
/* for OpenCV */
#include <opencv2/opencv.hpp>

/* for My modules */
#include "common_helper.h"
#include "common_helper_cv.h"
#include "inference_helper.h"
#include "feature_engine.h"

/*** Macro ***/
#define TAG "FeatureEngine"
#define PRINT(...)   COMMON_HELPER_PRINT(TAG, __VA_ARGS__)
#define PRINT_E(...) COMMON_HELPER_PRINT_E(TAG, __VA_ARGS__)


// #define MODEL_NAME  "model_float16_quant.tflite"
#define MODEL_NAME  "reid_model_0288.tflite"
#define TENSORTYPE  TensorInfo::kTensorTypeFp32
#define INPUT_NAME  "inputs"
#define INPUT_DIMS  { 1, 256, 128, 3 }
#define IS_NCHW     false
#define IS_RGB      false
#define OUTPUT_NAME "Identity"

static constexpr int32_t kNumFeature = 256;

/*** Function ***/
int32_t FeatureEngine::Initialize(const std::string& work_dir, const int32_t num_threads)
{
    /* Set model information */
    std::string model_filename = work_dir + "/model/" + MODEL_NAME;

    /* Set input tensor info */
    input_tensor_info_list_.clear();
    InputTensorInfo input_tensor_info(INPUT_NAME, TENSORTYPE, IS_NCHW);
    input_tensor_info.tensor_dims = INPUT_DIMS;
    input_tensor_info.data_type = InputTensorInfo::kDataTypeImage;
    input_tensor_info.normalize.mean[0] = 0.0f;
    input_tensor_info.normalize.mean[1] = 0.0f;
    input_tensor_info.normalize.mean[2] = 0.0f;
    input_tensor_info.normalize.norm[0] = 1.0f / 255.0f;
    input_tensor_info.normalize.norm[1] = 1.0f / 255.0f;
    input_tensor_info.normalize.norm[2] = 1.0f / 255.0f;
    input_tensor_info_list_.push_back(input_tensor_info);

    /* Set output tensor info */
    output_tensor_info_list_.clear();
    output_tensor_info_list_.push_back(OutputTensorInfo(OUTPUT_NAME, TENSORTYPE));

    /* Create and Initialize Inference Helper */
    //inference_helper_.reset(InferenceHelper::Create(InferenceHelper::kTensorflowLite));
    inference_helper_.reset(InferenceHelper::Create(InferenceHelper::kTensorflowLiteXnnpack));
    
    if (!inference_helper_) {
        return kRetErr;
    }
    if (inference_helper_->SetNumThreads(num_threads) != InferenceHelper::kRetOk) {
        inference_helper_.reset();
        return kRetErr;
    }
    if (inference_helper_->Initialize(model_filename, input_tensor_info_list_, output_tensor_info_list_) != InferenceHelper::kRetOk) {
        inference_helper_.reset();
        return kRetErr;
    }
    std::cout<<"Feature Engine initialization done!"<<std::endl;
    return kRetOk;
}

int32_t FeatureEngine::Finalize()
{
    if (!inference_helper_) {
        PRINT_E("Inference helper is not created\n");
        return kRetErr;
    }
    inference_helper_->Finalize();
    return kRetOk;
}


int32_t FeatureEngine::Process(const cv::Mat& original_mat, const std::vector<int>& bbox, Result& result,bool is_query)
{   
    std::string prepend;
    if(is_query){
        prepend = "./query/";
    }else{
        prepend = "./gallery/";
    }
    if (!inference_helper_) {
        PRINT_E("Inference helper is not created\n");
        return kRetErr;
    }

    InputTensorInfo& input_tensor_info = input_tensor_info_list_[0];

    /*** PreProcess ***/
    const auto& t_pre_process0 = std::chrono::steady_clock::now();
    
    int32_t crop_x = std::max(0, bbox[0]);
    int32_t crop_y = std::max(0, bbox[1]);
    int32_t crop_x1 = std::min(bbox[2]+bbox[0], original_mat.cols);
    int32_t crop_y1 = std::min(bbox[3]+bbox[1], original_mat.rows);
    int32_t crop_w = crop_x1-crop_x;
    int32_t crop_h = crop_y1-crop_y;
    // cv::rectangle(original_mat, cv::Rect(crop_x, crop_y, crop_w, crop_h), 0, 3);
    // cv::Mat img_ = original_mat(cv::Rect(crop_x, crop_y, crop_w, crop_h));
    // std::cout<<bbox[0]<<" "<<bbox[1]<<" "<<bbox[2]<<" "<<bbox[3]<<std::endl;
    // std::cout<<crop_x<<" "<<crop_y<<" "<<crop_w<<" "<<crop_h<<std::endl;
    cv::Mat img_src = cv::Mat::zeros(input_tensor_info.GetHeight(), input_tensor_info.GetWidth(), CV_8UC3);
    CommonHelper::CropResizeCvt(original_mat, img_src, crop_x, crop_y, crop_w, crop_h, IS_RGB, CommonHelper::kCropTypeStretch);
    // cv::imwrite(prepend+"test_"+std::to_string(rand()%10000)+".jpg",img_src);
    input_tensor_info.data = img_src.data;
    input_tensor_info.data_type = InputTensorInfo::kDataTypeImage;
    input_tensor_info.image_info.width = img_src.cols;
    input_tensor_info.image_info.height = img_src.rows;
    input_tensor_info.image_info.channel = img_src.channels();
    input_tensor_info.image_info.crop_x = 0;
    input_tensor_info.image_info.crop_y = 0;
    input_tensor_info.image_info.crop_width = img_src.cols;
    input_tensor_info.image_info.crop_height = img_src.rows;
    input_tensor_info.image_info.is_bgr = false;
    input_tensor_info.image_info.swap_color = false;
    if (inference_helper_->PreProcess(input_tensor_info_list_) != InferenceHelper::kRetOk) {
        return kRetErr;
    }
    const auto& t_pre_process1 = std::chrono::steady_clock::now();

    /*** Inference ***/
    const auto& t_inference0 = std::chrono::steady_clock::now();
    if (inference_helper_->Process(output_tensor_info_list_) != InferenceHelper::kRetOk) {
        return kRetErr;
    }
    const auto& t_inference1 = std::chrono::steady_clock::now();

    /*** PostProcess ***/
    const auto& t_post_process0 = std::chrono::steady_clock::now();
    float* raw_feature_list = output_tensor_info_list_[0].GetDataAsFloat();
    result.feature.reserve(kNumFeature);
    result.feature.assign(raw_feature_list, raw_feature_list + kNumFeature);
    const auto& t_post_process1 = std::chrono::steady_clock::now();
    result.time_pre_process += static_cast<std::chrono::duration<double>>(t_pre_process1 - t_pre_process0).count() * 1000.0;
    result.time_inference += static_cast<std::chrono::duration<double>>(t_inference1 - t_inference0).count() * 1000.0;
    result.time_post_process += static_cast<std::chrono::duration<double>>(t_post_process1 - t_post_process0).count() * 1000.0;

    return kRetOk;
}