/** 
Copyright 2021 iwatake2222

Licensed under the Apache License, Version 2.0 (the "License").
you may not use this file except in compliance with the License.

You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0


Modified by <Ye Yint Thu> (yeyintthu@globalwalkers.co.jp)
==============================================================================*/

#ifndef IMAGE_PROCESSOR_H_
#define IMAGE_PROCESSOR_H_

/* for general */
#include <cstdint>
#include <cmath>
#include <string>
#include <vector>
#include <array>
#include <map>
#include<chrono>
#include "detection_engine.h"

namespace cv {
    class Mat;
}

#define NUM_MAX_RESULT 50

namespace PersonTracking
{

typedef struct {
    char     work_dir[256];
    int32_t  num_threads;
} InputParam;

typedef struct {
    int32_t person_id;
    int32_t x_min;
    int32_t y_min;
    int32_t x_max;
    int32_t y_max;
} Result;

int32_t Initialize(const InputParam& input_param);
int32_t Process(cv::Mat& mat, std::map<std::string, std::vector<Result>>& result);
int32_t Finalize(void);
int32_t Command(int32_t cmd);

}

#endif