/** 
Copyright 2021 iwatake2222

Licensed under the Apache License, Version 2.0 (the "License").
you may not use this file except in compliance with the License.

You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0


Modified by - Ye Yint Thu (yeyintthu@globalwalkers.co.jp)
==============================================================================*/

/*** Include ***/
/* for general */
#include <cstdint>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <string>
#include <vector>
#include <array>
#include <algorithm>
#include <chrono>
#include <fstream>
#include <memory>

/* for OpenCV */
#include <opencv2/opencv.hpp>

/* for My modules */
#include "common_helper.h"
#include "common_helper_cv.h"
#include "bounding_box.h"
#include "person_tracking.h"
#include "get_time.hpp"
#include "BYTETracker.h"
/*** Macro ***/
#define TAG "PersonTracking"
#define PRINT(...)   COMMON_HELPER_PRINT(TAG, __VA_ARGS__)
#define PRINT_E(...) COMMON_HELPER_PRINT_E(TAG, __VA_ARGS__)
#define CSV_SOTRED_DIR CSV_DIR
#define FPS 30

/*** Global variable ***/
std::unique_ptr<DetectionEngine> s_det_engine;
// std::unique_ptr<FeatureEngine> s_feature_engine;
// TrackerDeepSort s_tracker(200);
std::unique_ptr<BYTETracker> tracker;

GetTime gt;

/*** Function ***/
static void DrawFps(cv::Mat& mat, double inference_fps,double time_inference_det, double time_inference_feature, double reid_time_taken,cv::Point pos, double font_scale, int32_t thickness, cv::Scalar color_front, cv::Scalar color_back, bool is_text_on_rect = true)
{
    char text[128];
    snprintf(text, sizeof(text), "FPS: %4.1f, Inference: DET: %4.1f[ms], TRACKING: %4.1f[ms], RE-ID: %4.1f[ms]", inference_fps, time_inference_det, time_inference_feature,reid_time_taken);
    CommonHelper::DrawText(mat, text, cv::Point(0, 0), 0.5, 2, CommonHelper::CreateCvColor(0, 0, 0), CommonHelper::CreateCvColor(180, 180, 180), true);
}

static cv::Scalar GetColorForId(int32_t id)
{
    static constexpr int32_t kMaxNum = 100;
    static std::vector<cv::Scalar> color_list;
    if (color_list.empty()) {
        std::srand(123);
        for (int32_t i = 0; i < kMaxNum; i++) {
            color_list.push_back(CommonHelper::CreateCvColor(std::rand() % 255, std::rand() % 255, std::rand() % 255));
        }
    }
    return color_list[id % kMaxNum];
}

int32_t PersonTracking::Initialize(const PersonTracking::InputParam& input_param)
{
    if (s_det_engine ) {
        PRINT_E("Already initialized\n");
        return -1;
    }
    s_det_engine.reset(new DetectionEngine(0.6f, 0.5f, 0.5f));
    if (s_det_engine->Initialize(input_param.work_dir, input_param.num_threads) != DetectionEngine::kRetOk) {
        s_det_engine->Finalize();
        s_det_engine.reset();
        return -1;
    }
    tracker.reset(new BYTETracker(FPS,30,input_param.work_dir,input_param.num_threads));
    /*
    s_feature_engine.reset(new FeatureEngine());
    if (s_feature_engine->Initialize(input_param.work_dir, input_param.num_threads) != FeatureEngine::kRetOk) {
        s_feature_engine->Finalize();
        s_feature_engine.reset();
        return -1;
    }
    */
    return 0;
}

int32_t PersonTracking::Finalize(void)
{
    if (!s_det_engine) {
        PRINT_E("Not initialized\n");
        return -1;
    }
    if (s_det_engine->Finalize() != DetectionEngine::kRetOk) {
        return -1;
    }
    /*
    if (s_feature_engine->Finalize() != FeatureEngine::kRetOk) {
        return -1;
    }
    */
    return 0;
}


int32_t PersonTracking::Command(int32_t cmd)
{
    if (!s_det_engine ) {
        PRINT_E("Not initialized\n");
        return -1;
    }

    switch (cmd) {
    case 0:
    default:
        PRINT_E("command(%d) is not supported\n", cmd);
        return -1;
    }
}

int32_t PersonTracking::Process(cv::Mat& mat, std::map<std::string, std::vector<PersonTracking::Result>>& result)
{   
    auto inference_start = chrono::system_clock::now();
    if (!s_det_engine) {
        PRINT_E("Not initialized\n");
        return -1;
    }

    /* Detection */
    DetectionEngine::Result det_result;
    if (s_det_engine->Process(mat, det_result) != DetectionEngine::kRetOk) {
        return -1;
    }
    
    /* Display detection result (black rectangle) */
    int32_t num_det = det_result.bbox_list.size();

    
   auto tracking_start = chrono::system_clock::now();
    std::vector<Object> persons;
    for(auto& box_info:det_result.bbox_list){
        Object person_obj;
        person_obj.prob = box_info.score;
        person_obj.label = 0;
        person_obj.rect.x=box_info.x;
        person_obj.rect.y=box_info.y;
        person_obj.rect.width=box_info.w;
        person_obj.rect.height=box_info.h;
        persons.push_back(person_obj);
    }
    double reid_time_consumed=0;
    std::vector<STrack> output_stracks = tracker->update(mat,persons,reid_time_consumed);
    
    int32_t num_track=0;
    for (int i = 0; i < output_stracks.size(); i++)
    {
        vector<float> tlwh = output_stracks[i].tlwh;
        int32_t crop_x = std::max(0, int(tlwh[0]));
        int32_t crop_y = std::max(0, int(tlwh[1]));
        int32_t crop_x1 = std::min(int(tlwh[2]+tlwh[0]), mat.cols);
        int32_t crop_y1 = std::min(int(tlwh[3]+tlwh[1]), mat.rows);
        int32_t crop_w = crop_x1-crop_x;
        int32_t crop_h = crop_y1-crop_y;
        Scalar s = tracker->get_color(output_stracks[i].track_id);
        // cv::circle(mat,cv::Point(int(crop_x+crop_w/2),int(crop_y+crop_h/2)),9,(255,0,125),-1);
        putText(mat, format("%d", output_stracks[i].track_id), Point(tlwh[0], tlwh[1] - 5), 
                0, 0.6, Scalar(0, 0, 255), 2, LINE_AA);
        rectangle(mat, Rect(crop_x, crop_y, crop_w, crop_h), s, 2);
        num_track++;
        

    }
    auto tracking_end = chrono::system_clock::now();
    auto time_tracking =static_cast<std::chrono::duration<double>>(tracking_end - tracking_start).count() * 1000.0;
    auto pure_tracking_time = time_tracking - reid_time_consumed;
    
    auto inference_fps = 1000/(static_cast<std::chrono::duration<double>>(tracking_end - inference_start).count()*1000);

    CommonHelper::DrawText(mat, "DET: " + std::to_string(num_det) + ", TRACK: " + std::to_string(num_track), cv::Point(0, 20), 0.7, 2, CommonHelper::CreateCvColor(0, 0, 0), CommonHelper::CreateCvColor(220, 220, 220));
    DrawFps(mat, inference_fps,det_result.time_inference, pure_tracking_time,reid_time_consumed, cv::Point(0, 0), 0.5, 2, CommonHelper::CreateCvColor(0, 0, 0), CommonHelper::CreateCvColor(180, 180, 180), true);
    
    return 0;
}