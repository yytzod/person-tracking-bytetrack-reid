/**
 * @file get_time.hpp
 * 
 * @author y-kishishita / created on 2021/07/12
 * @copyright 2021 GlobalWalkers.inc. All rights reserved.
 **/

#ifndef RETAIL_CAM__GET_TIME_HPP_
#define RETAIL_CAM__GET_TIME_HPP_

#include <ctime>
#include <iostream>
#include <stdio.h>
#include <cstdio>
#include <chrono>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <iomanip>
class GetTime {
  inline static char _datetime[32], _datetime_milli[32];

  static void set_nowtime();

  public:
    /** Constructor */
    GetTime()
    { 
      set_nowtime();
    }
    /** Getter for time with second */
    std::string const datetime() 
    { 
      set_nowtime();
      return std::string(_datetime); 
    }

    /** Getter for time with millisecond */
    std::string const datetime_milli() 
    { 
      set_nowtime();
      return std::string(_datetime_milli); 
    }
    
    inline std::string ConvertToTimeString(std::chrono::system_clock::time_point& time_point){
      auto epoch_seconds = std::chrono::system_clock::to_time_t(time_point);
      std::stringstream iso_stream;
      iso_stream << std::put_time(gmtime(&epoch_seconds), "%FT%T");
      auto truncated = std::chrono::system_clock::from_time_t(epoch_seconds);
      auto delta_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point - truncated).count();
      iso_stream << "." << std::fixed << std::setw(3) << std::setfill('0') << delta_ms<<"Z";

      return iso_stream.str();
    }
};


inline void GetTime::set_nowtime()
{
  struct timespec ts;
  struct tm t;
  int ret, msec;
  // Get epoch time
  ret = clock_gettime(CLOCK_REALTIME, &ts);
  if (ret < 0){
    perror("clock_gettime fail");
  }

  // Convert into local and parsed time
  localtime_r(&ts.tv_sec, &t);

  ret = strftime(_datetime, 32, "%Y/%m/%d %H:%M:%S", &t);
  if (ret == 0){
    perror("strftime fail");
  }

  // Add milli-seconds with snprintf
  msec = ts.tv_nsec / 1000000;
  ret = snprintf(_datetime_milli, 32, "%s.%03d", _datetime, msec);
  if (ret == 0){
    perror("snprintf fail");
  }
}

#endif  // RETAIL_CAM__GET_TIME_HPP_