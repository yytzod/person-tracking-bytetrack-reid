/** 
Copyright 2021 iwatake2222

Licensed under the Apache License, Version 2.0 (the "License").
you may not use this file except in compliance with the License.

You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0


Modified by <Ye Yint Thu> (yeyintthu@globalwalkers.co.jp)
==============================================================================*/

#ifndef INFERENCE_HELPER_LOG_
#define INFERENCE_HELPER_LOG_

/* for general */
#include <cstdint>
#include <cmath>
#include <string>
#include <vector>
#include <array>



#define INFERENCE_HELPER_LOG_PRINT_(...) printf(__VA_ARGS__)

#define INFERENCE_HELPER_LOG_PRINT(INFERENCE_HELPER_LOG_PRINT_TAG, ...) do { \
    INFERENCE_HELPER_LOG_PRINT_("[" INFERENCE_HELPER_LOG_PRINT_TAG "][%d] ", __LINE__); \
    INFERENCE_HELPER_LOG_PRINT_(__VA_ARGS__); \
} while(0);

#define INFERENCE_HELPER_LOG_PRINT_E(INFERENCE_HELPER_LOG_PRINT_TAG, ...) do { \
    INFERENCE_HELPER_LOG_PRINT_("[ERR: " INFERENCE_HELPER_LOG_PRINT_TAG "][%d] ", __LINE__); \
    INFERENCE_HELPER_LOG_PRINT_(__VA_ARGS__); \
} while(0);

#endif
