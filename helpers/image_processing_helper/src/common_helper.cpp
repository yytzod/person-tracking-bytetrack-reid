/** 
Copyright 2021 iwatake2222

Licensed under the Apache License, Version 2.0 (the "License").
you may not use this file except in compliance with the License.

You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0


Modified by <Ye Yint Thu> (yeyintthu@globalwalkers.co.jp)
==============================================================================*/

#include <cstdint>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <string>
#include <vector>
#include <array>
#include <algorithm>
#include <chrono>

#include "common_helper.h"

float CommonHelper::Sigmoid(float x)
{
    if (x >= 0) {
        return 1.0f / (1.0f + std::exp(-x));
    } else {
        return std::exp(x) / (1.0f + std::exp(x));    /* to aovid overflow */
    }
}

float CommonHelper::Logit(float x)
{
    if (x == 0) {
        return static_cast<float>(INT32_MIN);
    } else  if (x == 1) {
        return static_cast<float>(INT32_MAX);
    } else {
        return std::log(x / (1.0f - x));
    }
}