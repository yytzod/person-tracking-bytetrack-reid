#[[
    Written by <Ye Yint Thu> (yeyintthu@globalwalkers.co.jp)
    --------------------------------------------------------
]]

set(TENSORFLOW_DIR ${CMAKE_CURRENT_LIST_DIR}/../third_party/tf/tensorflow)
# add custom command for downloading tflite dependencies
add_custom_command(
    OUTPUT ${TENSORFLOW_DIR}/tensorflow/lite/tools/make/downloads
    COMMAND tensorflow/lite/tools/make/download_dependencies.sh
    WORKING_DIRECTORY ${TENSORFLOW_DIR}
)
# add the related target
add_custom_target(
    tensorflowlite_dependencies ALL
    DEPENDS ${TENSORFLOW_DIR}/tensorflow/lite/tools/make/downloads)
