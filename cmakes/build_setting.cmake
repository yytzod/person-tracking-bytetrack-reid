#[[ 
    Written by <Ye Yint Thu> (yeyintthu@globalwalkers.co.jp)
    --------------------------------------------------------
]]

cmake_minimum_required(VERSION 3.5)
# Select build system
set(BUILD_SYSTEM auto CACHE STRING "Build target? [auto, x64_linux, armv7, aarch64]")
if(${BUILD_SYSTEM} STREQUAL "auto")
    if(${CMAKE_SYSTEM_PROCESSOR} STREQUAL "armv7l")
        set(BUILD_SYSTEM armv7)
    elseif(${CMAKE_SYSTEM_PROCESSOR} STREQUAL "aarch64")
        set(BUILD_SYSTEM aarch64)
    else()
        set(BUILD_SYSTEM x64_linux)
    endif()
endif()
message("[main] CMAKE_SYSTEM_PROCESSOR = " ${CMAKE_SYSTEM_PROCESSOR} ", BUILD_SYSTEM = " ${BUILD_SYSTEM})
