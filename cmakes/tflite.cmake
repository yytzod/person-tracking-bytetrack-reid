#[[
    Written by <Ye Yint Thu> (yeyintthu@globalwalkers.co.jp)
    --------------------------------------------------------
]]

# specifies tensorflow lite include directories
set(THIRD_PARTY_DIR ${CMAKE_CURRENT_LIST_DIR}/../third_party)
set(TENSORFLOW_DIR ${THIRD_PARTY_DIR}/tf/tensorflow)
set(TFLITE_INCLUDE_DIR
    ${TENSORFLOW_DIR}
    ${TENSORFLOW_DIR}/tensorflow/lite/tools/make/downloads/flatbuffers/include
    ${TENSORFLOW_DIR}/tensorflow/lite/tools/make/downloads/absl
)
# select tflite shared library based on the build system
if(${BUILD_SYSTEM} STREQUAL "x64_linux")
    message("[tflite] for x64_linux")
    set(TFLITE_LIB_DIR ${THIRD_PARTY_DIR}/tf/tflite_prebuilt_shared_libs/x64_linux)
    set(TFLITE_LIB ${TFLITE_LIB_DIR}/libtensorflowlite.so)
elseif(${BUILD_SYSTEM} STREQUAL "armv7")
    message("[tflite] for armv7")
    set(TFLITE_LIB_DIR ${THIRD_PARTY_DIR}/tf/tflite_prebuilt_shared_libs/armv7)
    set(TFLITE_LIB ${TFLITE_LIB_DIR}/libtensorflowlite.so)
elseif(${BUILD_SYSTEM} STREQUAL "aarch64")
    message("[tflite] for aarch64")
    set(TFLITE_LIB_DIR ${THIRD_PARTY_DIR}/tf/tflite_prebuilt_shared_libs/aarch64)
    set(TFLITE_LIB ${TFLITE_LIB_DIR}/libtensorflowlite.so)
else()
    message(FATAL_ERROR "[tflite] unsupported platform")
endif()
# copy the tflite shared library  to the top level of build directory
file(COPY ${TFLITE_LIB} DESTINATION ${CMAKE_BINARY_DIR})
# install tflite shared library
install(FILES ${TFLITE_LIB} DESTINATION ${CMAKE_INSTALL_PREFIX}/lib)
