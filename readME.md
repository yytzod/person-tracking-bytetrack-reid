# README

This repository is for demonstrating person tracking by using Bytetrack which is integrated with re-identification model).

# Overview
This system is a combination of YOLOX Nano(for person detection) and Bytetrack(for person tracking) and a re-id network . So this system is generally a two-staged person tracking and (tracking + re-identification) system. The system first detects persons in the scene and generate bounding box for each of them. This generated bounding boxes are tracked by Bytetrack, and then the rest of un-tracked bounding boxes are processed by re-id networks. Generally, the re-id network aims to back up the Bytetrack by re-discovering the lost ids which cannot be processed by bytetrack.


# Usage

### Download tensorflow prebuilt shared libraries
- Please download [tflite_prebuilt_shared_libs](https://drive.google.com/file/d/1tqIclapJcToVaXHkYyUu4Go8_uQ7mWeq/view?usp=sharing) and extract it under **./third_party/tf** (create if not exist) folder.Then it will result the directory **third_party/tf/tflite_prebuilt_shared_libs/...**

## TFLite building

### Update Tensorflow git submodule
```
git submodule update --init --remote --progress
```

## How to build

### Install eigen in your system
Install eigen-3.3.9 [[google]](https://drive.google.com/file/d/1rqO74CYCNrmRAg8Rra0JP3yZtJ-rfket/view?usp=sharing), [[baidu(code:ueq4)]](https://pan.baidu.com/s/15kEfCxpy-T7tz60msxxExg).

```shell
unzip eigen-3.3.9.zip
cd eigen-3.3.9
mkdir build
cd build
cmake ..
sudo make install
```

### Download tflite weights for YOLOX and re-id models
- Please download [resource](https://drive.google.com/drive/folders/1fJkde9KGnmqkuwkHcjzsTrdwD0bKBGVh?usp=sharing) folder which contains **model** & **test-videos** sub-folders. Then, place this **resource** folder under the root directory of this repo.


## How to build and run the application
- Clone the repo
- Create a directory and build the application within it.
    ```sh
    mkdir build && cd build
    cmake ..
    make
    ```
- If building process is successful, run the application accordingly within the ***build*** directory,
    ```sh
    ./person_tracker [path to the testing video] # ./person_tracker resource/test-videos/TownCentreXVID.mp4
    ```
 
## References
- https://github.com/iwatake2222/play_with_tflite
- https://github.com/iwatake2222/InferenceHelper
- https://github.com/ifzhang/ByteTrack
